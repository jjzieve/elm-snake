import Color exposing (..)
import Graphics.Collage exposing (..)
import Graphics.Element exposing (..)
import Keyboard
import Markdown
import Time exposing (..)
import Window


-- MODEL

areaW = 600
areaH = 400

-- ## Snake Rules ##
-- Snake should be array of Models (segments)
-- where snake[0] should be head and snake[last] should be tail
-- Snake[n-1].xy @ t == Snake[n].xy @ t - 1

-- ## Snake Head Rules ##
-- if head touches walls, game over
-- if head touches anything in snake, game over
-- if head touches food, inc score, append to snake


type alias Model =
  { x : Float
  , y : Float
  , vx : Float
  , vy : Float
  , dir : String
  }


snake : Model
snake =
  Model 0 0 0 -10 "south"


-- UPDATE

update : (Time, { x:Int, y:Int }) -> Model -> Model
update (timeDelta, direction) model =
  model
    |> newVelocity direction
    |> setDirection direction
    |> updatePosition timeDelta


newVelocity : { x:Int, y:Int } -> Model -> Model
newVelocity {x,y} model =
  let
    scale = 5

    -- if new x input (right/left) arrow keys
    -- change the velocity accordingly and set
    -- y to zero (because we can only move like TRON)
    -- else, use old velocity (so snake keeps moving).
    -- Do vice-versa for y input
    newVelX = 
      if y == 0 then
        if x == 0 then
          model.vx
        else
          scale * toFloat x
      else
        0
        
    newVelY = 
      if x == 0 then
        if y == 0 then
          model.vy
        else
          scale * toFloat y
      else
        0
  in
      { model |
          vx = newVelX,
          vy = newVelY
      }


setDirection : { x:Int, y:Int } -> Model -> Model
setDirection {x,y} model =
  { model |
      dir =
        if x > 0 then
            "east"

        else if x < 0 then
            "west"

        else if y < 0 then
            "south"

        else if y > 0 then
            "north"

        else
            model.dir
  }


updatePosition : Time -> Model -> Model
updatePosition dt ({x,y,vx,vy} as model) =
  { model |
      x = clamp (-areaW/2) (areaW/2) (x + dt * vx),
      y = clamp (-areaH/2) (areaH/2) (y + dt * vy)
  }


-- VIEW

view : (Int,Int) -> Model -> Element
view (w,h) {x,y,vx,vy,dir} =
  container w h middle <|
  collage areaW areaH
    [ rect areaW areaH
        |> filled black
    , rect 22 28
        |> filled white
        |> move (x,y)
    ]


-- SIGNALS

main : Signal Element
main =
  Signal.map2 view Window.dimensions (Signal.foldp update snake input)


input : Signal (Time, { x:Int, y:Int })
input =
  Signal.sampleOn delta (Signal.map2 (,) delta Keyboard.arrows) -- the (,) must indicate anonymous function


delta : Signal Time
delta =
  Signal.map (\t -> t / 20) (fps 35)
