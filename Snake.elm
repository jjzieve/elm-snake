-- See this document for more information on making Pong:
import Color exposing (..)
import Graphics.Collage exposing (..)
import Graphics.Element exposing (..)
import Keyboard
import Text
import Time exposing (..)
import Window
import List

-- MODEL

(gameWidth,gameHeight) = (600,400)

type alias Snake =
  { x : Float
  , y : Float
  , vx : Float
  , vy : Float
  }

type alias Game =
  { snake : Snake }


default_game : Game
default_game =
  { snake = Snake 0 0 0 0}


type alias Input =
  { dir1 : Int -- x value {0,1,-1}
  , dir2 : Int -- y value {0,1,-1}
  , delta : Time
  }


 -- UPDATE
 -- https://github.com/Dobiasd/Maze/blob/master/Main.elm use Touch?
-- score =
--       if food.x == snake.x && food.y == snake.y then 1 else 0

update : Input -> Game -> Game
update {dir1,dir2,delta} ({snake} as game) =
  snake
    |> updatePosition delta
    -- { game | 
    --     snake = updateSnake delta dir1 dir2 snake 
    -- }
    
-- update : (Time, { x:Int, y:Int }, Bool) -> Model -> Model
-- update (timeDelta, direction, isRunning) model =
--   model
--     |> newVelocity isRunning direction
--     |> setDirection direction
--     |> updatePosition timeDelta
-- ** pseudocode **
-- if no input:
--   inc/dec position based on current non-zero velocity
-- else:
--   update current non-zero velocity
--   recursive call
  
updateSnake : Time -> Int -> Int -> Snake -> Snake
updateSnake delta dir1 dir2 snake =
    if dir1 == 0 && dir2 == 0 then
      if snake.vx /= 0 then
        { snake | x = snake.x + snake.vx * delta }
      else
        { snake | y = snake.y + snake.vy * delta }
    else
      if dir1 /= 0 then
          { snake | x = snake.x + snake.vx * delta }
      else
          { snake | y = snake.y + snake.vy * delta }

updatePosition : Time -> Snake -> Snake
updatePosition dt ({x,y,vx,vy} as snake) =
  { snake |
      x = clamp (-gameWidth/2) (gameWidth/2) (x + dt * vx),
      y = clamp (-gameHeight/2) (gameHeight/2) (y + dt * vy)
  }

physicsUpdate dt obj =
  { obj |
      x = obj.x + obj.vx * dt,
      y = obj.y + obj.vy * dt
  }

  
-- ate food snake =
--   food.x == snake.x && snake.y == food.y 

 -- VIEW

view : (Int,Int) -> Game -> Element
view (w,h) game =
  let
    scores =
      txt (Text.height 50) (toString game.snake.x)
  in
    container w h middle <|
    collage gameWidth gameHeight
      [ rect gameWidth gameHeight
          |> filled black
        , rect 20 60
          |> make game.snake
        , toForm scores
          |> move (0, gameHeight/2 - 40)
      ]
      
make obj shape =
  shape
    |> filled white
    |> move (obj.x, obj.y)
    
textGreen =
  rgb 160 200 160
    
txt f string =
  Text.fromString string
    |> Text.color white
    |> Text.monospace
    |> f
    |> leftAligned

-- SIGNALS

main =
  Signal.map2 view Window.dimensions gameState

-- main = Signal.map view Window.dimensions

gameState : Signal Game
gameState =
  Signal.foldp update default_game input

delta =
  Signal.map inSeconds (fps 35)

input : Signal Input
input =
  Signal.sampleOn delta <|
    Signal.map3 Input
      (Signal.map .x Keyboard.arrows)
      (Signal.map .y Keyboard.arrows)
      delta
